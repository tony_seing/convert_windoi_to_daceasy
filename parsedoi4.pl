use XML::CSV;
use XML::SAX;
use Text::CSV;
use XML::Simple;
use Data::Dumper;
#use Date::Format;

# naming of the input file and output file
my $file = 'orders.csv';
print "Input filename of XML file to be imported to WinDOI: ";
$file = <STDIN>;
chop($file);
print "Input filename of doi file to be outputted: ";
$outf = <STDIN>;
chop($outf);


# my $csv = Text::CSV->new();
open ORDERS, $file;
# @a = <ORDERS>;
# my $status = $csv->parse($a[0]);
# my @fields = $csv->fields();


open OUTFILE, ">$outf";

			   
# create object
 $xml = new XML::Simple(suppressempty => '',   forcearray => 1);

# read XML file
$ data = $xml->XMLin($file);

 

# print output
$max = scalar(@{$data->{OrderList}});
$i = 1;

$previous = -999999;	




$i = 0;
$headercount = 0;
$shipcost = 0.00;	
$handlingcost = 0.00;
			
# loop through every row of the xml file and print out header and detail fields for every column of data
while  ($i < $max) #print all
{
	
		$headercount++;
		$billphone = $data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{Phone}->[0];
		$billphone =~ s/-//g;
		$billphone =~ s/\(//g;
		$billphone =~ s/\)//g;
		$billphone =~ s/\.//g;
		$billphone =~ s/ //g;
		$billphone =~ s/[A-Z]//g;
		$billphone =~ s/[a-z]//g;
		$billphone =~ s/#//g;
		
		print OUTFILE '"' . $billphone . '"' . ","; #customer code
		print OUTFILE '"O","N",'; # order type, order status
		print OUTFILE '"' . $data->{OrderList}->[$i]->{OrderId} . '",'; # customer ref
		print OUTFILE '"",'; # media
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Shipping}->[0]->{Name}->[0] . '",';  #shipping method
		print OUTFILE '"PREPAID",'; #FOB
		print OUTFILE '"",'; # (blank)
		
		# print OUTFILE '"' . $data->{OrderList}->[$i]->{Tracking_Number}->[0] . '",'; #reference number
		# print blank instead of shipping reference number...doesn't appear to be any for DGS...
		print OUTFILE '"",'; # (blank)
			
		print OUTFILE ','; #blank
		$shipcost = $data->{OrderList}->[$i]->{Invoice}->[0]->{Handling}->[0]->{content};
		$handlingcost = $data->{OrderList}->[$i]->{Invoice}->[0]->{Shipping}->[0]->{content};
		print OUTFILE "" . $shipcost + $handlingcost . ','; #freight
		
		
		print OUTFILE ','; #blank
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{FirstName}->[0] . " " . $data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{LastName}->[0] . '"' . ","; #name
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{Company}->[0] . '"' . ","; #contact
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{Address1}->[0] . '",'; #address1
		print OUTFILE '"",'; #address2 - theres no address 2 in new system
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{City}->[0] . '",'; #city
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{StateProvince}->[0] . '",'; #state
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{PostalCode}->[0] . '",'; #zip code
		print OUTFILE '"' . $billphone . '",'; #phone number
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{Country}->[0]  . '",'; #country
		print OUTFILE '"' . $data->{OrderList}->[$i]->{OrderId} . '",'; # order id again
		if(uc($data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{StateProvince}->[0]) eq "MA")
		{
			print OUTFILE '"MASS",';
	
		}
		elsif  (uc($data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{StateProvince}->[0]) eq "CT") # tax code
		{
				print OUTFILE '"CT",';
		}
		elsif (uc($data->{OrderList}->[$i]->{Customer}->[0]->{BillingAddress}->[0]->{StateProvince}->[0]) eq "NJ")
		{
			print OUTFILE '"NJ",';
		}
		else
		{
			print OUTFILE '"0",';
		}
		
		print OUTFILE '"CASH",'; # terms code
		print OUTFILE '"WEB",'; #salesperson
		
		@orderdate =  split(/ /, substr($data->{OrderList}->[$i]->{CreateDate}->[0],0,10)); # substr returns the first ten characters of date/time...ensuring that only date is entered
		@dateparts = split(/-/, @orderdate[0]);
		
		# reading from xml...no longer need this algorithm to fix up date strings
		#format date correctly
		#if ($dateparts[1] < 10)
		#{
		#	$mm = "0" . $dateparts[0];
		#}
		#else
		#{
			$mm = $dateparts[1];
		#}
		
		#if ($dateparts[2] < 10)
		#{
		#	$dd = "0" . $dateparts[2];
		#}
		#else
		#{
			$dd = $dateparts[2];
		#}
		$yy = substr($dateparts[0],2,3);
		
		
		$formattedDate = $mm . "/" . $dd . "/" . $yy;
		
		
		print OUTFILE '"' . $formattedDate . '",'; #order date
		print OUTFILE  '"' . $formattedDate . '",' . ""; #required  date
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{EmailAddress}->[0] . '"' . "\n";
		
	




	# print output
	$maxItems = scalar(@{$data->{OrderList}->[$i]->{Invoice}->[0]->{LineItemList}});
	$k = 0;
	
	# loop through all items bought by user and print them to OUTFILE	
	while ($k < $maxItems)
	{
				
		#now print out detail field
		#################################
		print OUTFILE '"",'; #customer code
		print OUTFILE '"3","",'; # detail type, detail status
		$shiphone = $data->{OrderList}->[$i]->{Customer}->[0]->{ShippingAddress}->[0]->{Phone}->[0];
		
		$shiphone =~ s/-//g;
		$shiphone =~ s/\(//g;
		$shiphone =~ s/\)//g;
		$shiphone =~ s/\.//g;
		$shiphone =~ s/ //g;
		$shiphone =~ s/[A-Z]//g;
		$shiphone =~ s/[a-z]//g;
		$shiphone =~ s/#//g;
		print OUTFILE '"","","","",'; # (blank)x4
		$partdescription = $data->{OrderList}->[$i]->{Invoice}->[0]->{LineItemList}->[$k]->{Name}->[0];
		$partdescription =~ s/"/inch/g;
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Invoice}->[0]->{LineItemList}->[$k]->{PartNumber}->[0] . '",';  #detail code
		print OUTFILE '"' . $partdescription . '",'; #description
		print OUTFILE $data->{OrderList}->[$i]->{Invoice}->[0]->{LineItemList}->[$k]->{QtySold}->[0] . ','; # order quantity
		print OUTFILE $data->{OrderList}->[$i]->{Invoice}->[0]->{LineItemList}->[$k]->{UnitPrice}->[0]->{content} . ','; # unit price
		print OUTFILE ","; #line discount
	
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{ShippingAddress}->[0]->{FirstName}->[0] . " " . $data->{OrderList}->[$i]->{Customer}->[0]->{ShippingAddress}->[0]->{LastName}->[0] . '"' . ","; #name
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{ShippingAddress}->[0]->{Company}->[0] . '",'; #contact
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{ShippingAddress}->[0]->{Address1}->[0] . '",'; #address1
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{ShippingAddress}->[0]->{Address2}->[0] . '",'; #address2
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{ShippingAddress}->[0]->{City}->[0] . '",'; #city
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{ShippingAddress}->[0]->{StateProvince}->[0] . '",'; #state
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{ShippingAddress}->[0]->{PostalCode}->[0] . '",'; #zip code
		print OUTFILE '"' . $shiphone . '",'; #phone number
		
		print OUTFILE '"' . $data->{OrderList}->[$i]->{Customer}->[0]->{ShippingAddress}->[0]->{Country}->[0] . '",'; #country
		print OUTFILE '"","","","","","",""' . "\n"; # (blank)x7     28th field added per zac's request
	
		# print out D line field
		$nexti = $i +1;
		if ($k + 1 == $maxItems)
		{
			print OUTFILE '"","5","","",'; # (blank)x4
			$partdescription = $data->{OrderList}->[$i]->{Shipping}->[0]->{Name}->[0]; # shipping method
			print OUTFILE '"","","",'; # (blank)x3
			print OUTFILE '"' . "D" . '",';  #detail code
			print OUTFILE '"' . $partdescription . '",'; #description
			print OUTFILE ','; # order quantity
			print OUTFILE ','; # unit price
			print OUTFILE ','; #line discount
		
			print OUTFILE '"",'; #name
			print OUTFILE '"",'; #contact
			print OUTFILE '"",'; #address1
			print OUTFILE '"",'; #address2
			print OUTFILE '"",'; #city
			print OUTFILE '"",'; #state
			print OUTFILE '"",'; #zip code
			print OUTFILE '"",'; #phone number
			print OUTFILE '"",'; #country
			print OUTFILE '"","","","","","",""' . "\n"; # (blank)x7       28th field added per zac's request
		}
		$k++;
	}

	
	
	$i++; #increment index of iterator
	
	
	
	
}	
close OUTFILE;
close ORDERS;
print "Conversion completed. \n";
$hold = <STDIN>;
